from django import template
from django.contrib.messages import get_messages
from django.utils.translation import ugettext as _
register = template.Library()


@register.inclusion_tag('mothership/message.html', takes_context=True)
def message(context, *args, **kwargs):
    """
    This template tag allows us to have:
    {% messages 'password-changed' 'password-problem' with contained=True|False top_margin=True|False bottom_margin=True|False type=success|info|notice|error unclosable=True heading=True title='...' body='...'  %}
    Instead the following in our templates:
    {% if messages %}
        {% for message in messages %}
            {% if message.extra_tags == 'password-changed' %}
                {# [{{ message.tags }}] #}
                {% include 'mothership/message.html' with type='positive' title='Success' content=message closeable=True %}
            {% elif message.extra_tags == 'settings_password-problem' %}
                {% include 'mothership/message.html' with type='negative' title='Invalid Submission' content=message closeable=True %}
            {% endif %}
        {% endfor %}
    {% endif %}
    """

    storage = get_messages(context.request)

    for msg in storage:
        # msg.extra_tags contains the message name only
        # We should probably used level and pull the mapping 'from django.contrib.messages.constants import DEFAULT_TAGS' instead of splitting
        msg_tags = msg.tags.split(' ')
        # This is the extra_tag we add from views
        msg_id = msg_tags[0]
        # This is the message type we specify while creating a message; message.success(... message.info(...
        msg_type = msg_tags[1]

        if msg_type == 'error' and msg.message == 'default':
            msg = _('There was a problem in your submission.')

        # Make the error title a little bit more friendly; Error -> Oops
        if msg_type == 'error':
            msg_title = _('Oops')
        else:
            msg_title = msg_type

        if msg_id in args:
            return {
                'type': msg_type,
                'title': msg_title,
                'body': msg,
                'contained': kwargs.get('contained', False),
                'top_margin': kwargs.get('contained', False),
                'bottom_margin': kwargs.get('contained', False),
                **kwargs  # Allows the ability to pass variables via 'with': {% message 'profile-updated' with bottom_margin=True %}
            }

    return {
        'no_message': True
    }
