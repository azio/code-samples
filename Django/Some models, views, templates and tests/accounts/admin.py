from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from accounts.models import Profile


# Define an inline admin descriptor for Profile model
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    # verbose_name_plural = 'collaborator'


class TokenInline(admin.StackedInline):
    model = Token
    can_delete = False


# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (ProfileInline, TokenInline)


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


@admin.register(Profile)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'full_name')
