from django.contrib.auth.models import User
from django.test import TestCase

from .forms import UserSignUpForm, ProfileSignUpForm, LoginForm, PasswordRecoveryForm, PasswordResetForm, PasswordChangeForm, ProfileForm, EmailForm


class UserSignUpFormTests(TestCase):
    fixtures = ('users',)

    def test_blank_data(self):
        f = UserSignUpForm({})

        self.assertEqual(f.errors['password'], ['This field is required.'])
        self.assertEqual(f.errors['email'], ['This field is required.'])

    def test_email_uniqueness(self):
        f = UserSignUpForm({'email': 'acme@example.com'})

        self.assertFalse(f.is_valid())
        self.assertEqual(f.errors['email'], ['Email is already in use.'])

    def test_valid_data(self):
        f = UserSignUpForm({'password': 'secret', 'email': 'leela@example.com'})

        self.assertTrue(f.is_valid())

        user = f.save()

        self.assertEqual(user.password, 'secret')
        self.assertEqual(user.email, 'leela@example.com')


class ProfileSignUpFormTests(TestCase):
    def test_blank_data(self):
        f = ProfileSignUpForm({})

        self.assertEqual(f.errors['full_name'], ['This field is required.'])

    def test_valid_data(self):
        f = ProfileSignUpForm({'full_name': 'Turanga Leela'})
        self.assertTrue(f.is_valid())


class LoginFormTests(TestCase):
    def test_blank_data(self):
        f = LoginForm({})

        self.assertEqual(f.errors['email'], ['This field is required.'])
        self.assertEqual(f.errors['password'], ['This field is required.'])

    def test_valid_data(self):
        f = LoginForm({'email': 'sam', 'password': 'secret'})

        self.assertTrue(f.is_valid())


class PasswordRecoveryFormTests(TestCase):
    def test_blank_data(self):
        f = PasswordRecoveryForm({})

        self.assertEqual(f.errors['email'], ['This field is required.'])

    def test_valid_data(self):
        f = PasswordRecoveryForm({'email': 'lara@example.com'})

        self.assertTrue(f.is_valid())


class PasswordResetFormTests(TestCase):
    def test_blank_data(self):
        f = PasswordResetForm({})

        self.assertEqual(f.errors['password'], ['This field is required.'])

    def test_valid_data(self):
        f = PasswordResetForm({'password': '12345678'})

        self.assertTrue(f.is_valid())


class PasswordChangeFormTests(TestCase):
    fixtures = ('users',)

    def setUp(self):
        self.user = User.objects.get(pk=1)

    def test_blank_data(self):
        f = PasswordChangeForm({})

        self.assertEqual(f.errors['password'], ['This field is required.'])
        self.assertEqual(f.errors['password_confirmation'], ['This field is required.'])
        self.assertEqual(f.errors['current_password'], ['This field is required.'])

    def test_password_length_restriction(self):
        f = PasswordChangeForm({
            'password': '12345678',
            'password_confirmation': '123',
            'current_password': 'secret'
        })

        self.assertEqual(f.errors['password_confirmation'], ['Ensure this value has at least 6 characters (it has 3).'])

    def test_password_confirmation_mismatch(self):
        f = PasswordChangeForm({
            'password': '12345678',
            'password_confirmation': '87654321',
            'current_password': 'secret'
        }, instance=self.user)

        self.assertEqual(f.errors['password_confirmation'], ['Password and password confirmation must match.'])

    def test_incorrect_current_password(self):
        f = PasswordChangeForm({
            'password': '12345678',
            'password_confirmation': '87654321',
            'current_password': '123123123'
        })

        self.assertEqual(f.errors['current_password'], ['Current password is invalid.'])

    def test_valid_data(self):
        f = PasswordChangeForm({
            'password': '12345678',
            'password_confirmation': '12345678',
            'current_password': 'secret'
        }, instance=self.user)

        self.assertTrue(f.is_valid())


class ProfileFormTests(TestCase):
    fixtures = ('users',)

    def setUp(self):
        self.user = User.objects.get(pk=1)

    def test_blank_data(self):
        f = ProfileForm({})

        self.assertEqual(f.errors['full_name'], ['This field is required.'])

    def test_phone_validation(self):
        f = ProfileForm({
            'full_name': 'Joe Doe',
            'phone': 'invalid number'
        }, instance=self.user)

        self.assertEqual(f.errors['phone'], ['The phone number format is incorrect.'])

    def test_valid_data(self):
        f = ProfileForm({
            'full_name': 'Joe Doe',
            'phone': '123-3456'  # Dashes will be stripped by the form's phone cleanup method
        }, instance=self.user)

        self.assertTrue(f.is_valid())
        self.assertEqual(f.cleaned_data['phone'], '1233456')  # This must come after is_value call above to avoid "AttributeError: 'ProfileForm' object has no attribute 'cleaned_data'"


class EmailFormTests(TestCase):
    def test_blank_data(self):
        f = EmailForm({})

        self.assertEqual(f.errors['new_email'], ['This field is required.'])

    def test_invalid_data(self):
        f = EmailForm({'new_email': 'invalid_email'})

        self.assertEqual(f.errors['new_email'], ['Enter a valid email.'])

    def test_valid_data(self):
        f = EmailForm({'new_email': 'someone@example.com'})

        self.assertTrue(f.is_valid())
