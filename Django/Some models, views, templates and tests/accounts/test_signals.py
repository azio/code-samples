from django.contrib.auth.models import User
from django.test import TestCase

from .models import Profile
from uuid import UUID


class AccountsSignalTests(TestCase):
    def test_create_auth_token(self):
        """ Test token being created upon user creation """

        u = User.objects.create(username='summer', password='secret')

        self.assertEqual(len(u.auth_token.key), 40)

    def test_set_username(self):
        """ Test username is set to UUID when it's left empty """

        u = User.objects.create(email='sarah@eexample.com', password='secret')

        try:
            UUID(str(u.username), version=4)
        except ValueError:
            self.fail('Empty username was not set to a valid UUID by the signal')

    def test_create_user_profile(self):
        """ Test user profile being created upon user creation """

        u = User.objects.create(username='maya', password='secret')

        self.assertTrue(isinstance(u.profile, Profile))
        self.assertTrue(Profile.objects.filter(user__username=u.username).exists())

    def test_set_initial_email(self):
        """ Test profile first_email field being set upon profile creation """

        u = User.objects.create(username='tina', password='secret', email='tina@example.com')

        self.assertEqual(u.profile.first_email, 'tina@example.com')
