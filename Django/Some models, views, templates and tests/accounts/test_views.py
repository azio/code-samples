import uuid
from datetime import timedelta

from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase, Client
from django.utils import timezone
from django_webtest import WebTest

from mothership.utils.string import random_string
from mothership.utils.url import ext_reverse
from .models import Profile


# The Django test client cannot test form submissions, but WebTest can. We use django-webtest to handle testing the form submission

class SignUpAndCreateOrganizationTests(WebTest):
    def setUp(self):
        self.page = self.app.get(ext_reverse('accounts:signup', 'organization'))

    def test_status_code(self):
        self.assertEqual(self.page.status_code, 200)

    def test_form_presence(self):
        self.assertEqual(len(self.page.forms), 1)

    def test_invalid_form(self):
        page = self.page.form.submit()

        self.assertContains(page, 'This field is required.', count=4)

    def test_profile_data(self):
        self.page.form['name'] = 'Acme'
        self.page.form['full_name'] = 'Jack Ron'
        self.page.form['email'] = 'jack@example.com'
        self.page.form['password'] = 'secret'

        self.page.form.submit()

        u = User.objects.get(email='jack@example.com')

        self.assertEqual(u.profile.full_name, 'Jack Ron')
        self.assertEqual(u.profile.ip_address, '127.0.0.1')
        self.assertEqual(len(u.profile.activation_key), 16)

    def test_activation_url(self):
        self.page.form['name'] = 'Acme'
        self.page.form['full_name'] = 'Nate Diaz'
        self.page.form['email'] = 'nate@example.com'
        self.page.form['password'] = 'secret'

        page = self.page.form.submit()

        p = Profile.objects.get(user__email='nate@example.com')

        activation_url = 'https://www.acme.com%s' % reverse(
            'accounts:activate', kwargs={
                'profile_pk': p.pk,
                'activation_key': p.activation_key
            }
        )

        self.assertEqual(activation_url, page.context['activation_url'])

    def test_valid_form(self):
        self.page.form['name'] = 'Acme'
        self.page.form['full_name'] = 'Martha Stewart'
        self.page.form['email'] = 'martha.stewart@example.com'
        self.page.form['password'] = 'secret'

        page = self.page.form.submit()

        self.assertTemplateUsed(page, 'mothership/naked_message.html')

        self.assertContains(page, "Your new account has been successfully created. However, it will not be activated until you click the link in the activation email you'll shortly receive at martha.stewart@example.com.")


class SignUpWithoutCreatingOrganizationTests(WebTest):
    def setUp(self):
        self.page = self.app.get(ext_reverse('accounts:signup', 'personal'))

    def test_valid_form(self):
        self.page.form['full_name'] = 'Martha Stewart'
        self.page.form['email'] = 'martha.stewart@example.com'
        self.page.form['password'] = 'secret'

        page = self.page.form.submit()

        self.assertTemplateUsed(page, 'mothership/naked_message.html')

        self.assertContains(page, "Your new account has been successfully created. However, it will not be activated until you click the link in the activation email you'll shortly receive at martha.stewart@example.com.")


class SignInTests(WebTest):
    def setUp(self):
        self.page = self.app.get(ext_reverse('accounts:login'))

    def test_status_code(self):
        self.assertEqual(self.page.status_code, 200)

    def test_form_presence(self):
        self.assertEqual(len(self.page.forms), 1)

    def test_required_fields(self):
        """ Make sure an error is shown when no user/password is provided """

        self.page.form['email'] = ''
        self.page.form['password'] = ''

        page = self.page.form.submit()

        self.assertContains(page, 'The provided credentials are invalid.')

    def test_error_message_on_invalid_credentials(self):
        """ Make sure an error is shown when an invalid login credentials is provided """

        self.page.form['email'] = 'unexisting_user@example.com'
        self.page.form['password'] = 'unexisting_password'

        page = self.page.form.submit()

        self.assertContains(page, 'The provided credentials are invalid.')

    def test_blockage_of_inactive_accounts(self):
        """ Make sure accounts with is_active field set to false are are not allowed to log in even if the credentials are valid """

        user = User.objects.create_user(username='sasha', email='sasha@example.com', password='secret', is_active=False)

        self.page.form['email'] = 'sasha@example.com'
        self.page.form['password'] = 'secret'

        page = self.page.form.submit()

        self.assertContains(page, 'The provided credentials are invalid.')

    def test_blockage_of_accounts_with_unverified_emails(self):
        """ Make sure accounts with unverified email addresses are not allowed to log in even if the credentials are valid """

        user = User.objects.create_user(username='zoidberg', email='zoidberg@example.com', password='secret')

        self.page.form['email'] = 'zoidberg@example.com'
        self.page.form['password'] = 'secret'

        page = self.page.form.submit()

        self.assertContains(page, 'This account has not been activated yet. Please check your email to find the activation instructions.')

    def test_redirection(self):
        """ Ensure 'next' query string is appended to the login URI """

        response = self.client.get(ext_reverse('organizations:create'))
        self.assertRedirects(response, ext_reverse('accounts:login') + '?next=' + ext_reverse('organizations:create'))

    def test_valid_submission(self):
        """ Makes sure login works when given correct credentials and the user is redirected to the create organization if he doesn't belong to any organization """

        user = User.objects.create_user(username='sam', email='sam@example.com', password='secret')

        user.profile.is_email_confirmed = True
        user.profile.save()

        self.page.form['email'] = 'sam@example.com'
        self.page.form['password'] = 'secret'

        page = self.page.form.submit().follow()

        self.assertRedirects(page, ext_reverse('organizations:create'))


class SignOutTests(TestCase):
    fixtures = ('users', 'profiles')

    def setUp(self):
        self.client = Client()

    def test_sign_out(self):
        # Sign in and ensure we are actually in
        self.client.login(username='acme@example.com', password='secret')

        response = self.client.get(ext_reverse('organizations:create'))

        self.assertFalse(isinstance(response.context['user'], AnonymousUser))
        self.assertEqual(response.context['user'].email, 'acme@example.com')

        # Sign out
        response = self.client.get(ext_reverse('accounts:logout'))  # A more realistic approach than self.client.logout()
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, ext_reverse('website:home'))

        # See if we are an anonymous user now
        response = self.client.get(reverse('website:home'))
        self.assertTrue(isinstance(response.context['user'], AnonymousUser))


class ActivateTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_already_activated(self):
        u = User.objects.create(username='lana', password='123')
        u.profile.is_email_confirmed = True
        u.profile.save()

        activation_uri = ext_reverse('accounts:activate', kwargs={
            'profile_pk': u.profile.pk,
            'activation_key': 'abc'
        })

        response = self.client.get(activation_uri)

        self.assertContains(response, 'This account has been already activated.')

    def test_successful_activation(self):
        u = User.objects.create(username='maya', email='maya@example.com', password='123')
        u.profile.activation_key = 'abc'
        u.profile.save()

        activation_uri = ext_reverse('accounts:activate', kwargs={
            'profile_pk': u.profile.pk,
            'activation_key': u.profile.activation_key
        })

        response = self.client.get(activation_uri, follow=True)

        self.assertRedirects(response, ext_reverse('organizations:create'))

        u.profile.refresh_from_db()  # A must

        self.assertTrue(u.profile.is_email_confirmed)
        self.assertEqual(u.profile.activation_key, '')

    def test_invalid_activation_request(self):
        u = User.objects.create(username='george', password='123')
        u.profile.activation_key = 'xyz'
        u.profile.save()

        activation_uri = reverse('accounts:activate', kwargs={
            'profile_pk': u.profile.pk,
            'activation_key': 'abc'  # instead of xyz above
        })

        response = self.client.get(activation_uri)

        self.assertContains(response, 'The activation URL is invalid. Please click on the activation link sent to your email account.')


class RecoverTests(WebTest):
    fixtures = ('users', 'profiles')

    def setUp(self):
        self.page = self.app.get(ext_reverse('accounts:recover'))

    def test_status_code(self):
        self.assertEqual(self.page.status_code, 200)

    def test_form_presence(self):
        self.assertEqual(len(self.page.forms), 1)

    def test_required_fields(self):
        self.page.form['email'] = ''

        page = self.page.form.submit()

        self.assertContains(page, 'This field is required.')

    def test_unrecognized_email(self):
        self.page.form['email'] = 'nobody@example.com'

        page = self.page.form.submit()

        self.assertContains(page, 'Unrecognized email address.')

    def test_reset_url(self):
        self.page.form['email'] = 'acme@example.com'

        page = self.page.form.submit()

        p = Profile.objects.get(user__email='acme@example.com')

        self.assertEqual(
            page.context['password_reset_url'],
            'https://www.acme.com{uri}'.format(
                uri=ext_reverse(
                    'accounts:reset', kwargs={
                        'profile_pk': p.pk,
                        'reset_key': p.password_reset_key
                    }
                )
            )
        )

    def test_valid_submission(self):
        email = 'acme@example.com'

        self.page.form['email'] = email

        self.assertEqual(len(User.objects.get(email=email).profile.password_reset_key), 0)

        page = self.page.form.submit()

        self.assertContains(page, 'Instructions for resetting your password have just been emailed to you.')
        self.assertEqual(len(User.objects.get(email=email).profile.password_reset_key), 16)


class ResetTests(WebTest):
    fixtures = ('users', 'profiles')

    def setUp(self):
        self.user = User.objects.get(email='acme@example.com')

        self.profile = self.user.profile

    def test_disabled_accounts(self):
        self.user.is_active = False

        self.user.save()

        self.profile.password_reset_key = random_string(16)
        self.profile.password_reset_date = timezone.now()

        self.profile.save()

        page = self.app.get(ext_reverse(
            'accounts:reset', kwargs={
                'profile_pk': self.profile.pk,
                'reset_key': self.profile.password_reset_key
            }
        ))

        self.assertEqual(page.status_code, 200)
        self.assertContains(page, 'This account has been suspended. This may be caused by either a violation of the Terms of Use or for the purpose of verifying your membership.')

    def test_expired_requests(self):
        self.profile.password_reset_key = random_string(16)
        self.profile.password_reset_date = timezone.now() - timedelta(days=2)  # 2 days ago

        self.profile.save()

        page = self.app.get(ext_reverse(
            'accounts:reset', kwargs={
                'profile_pk': self.profile.pk,
                'reset_key': self.profile.password_reset_key
            }
        ))

        self.assertEqual(page.status_code, 200)
        self.assertContains(page, 'This password reset request has expired. Feel free to send a new request if you still desire to change it.')

    def test_invalid_reset_keys(self):
        self.profile.password_reset_key = random_string(16)
        self.profile.password_reset_date = timezone.now()

        self.profile.save()

        page = self.app.get(ext_reverse(
            'accounts:reset', kwargs={
                'profile_pk': self.profile.pk,
                'reset_key': random_string(16)
            }
        ))

        self.assertEqual(page.status_code, 200)
        self.assertContains(page, 'The provided reset key is invalid or has been previously used.')

    def test_not_found_profiles(self):
        self.profile.password_reset_key = random_string(16)
        self.profile.password_reset_date = timezone.now()

        self.profile.save()

        page = self.app.get(ext_reverse(
            'accounts:reset', kwargs={
                'profile_pk': uuid.uuid4(),
                'reset_key': random_string(16)
            }
        ))

        self.assertEqual(page.status_code, 200)
        self.assertContains(page, 'The reset URL is invalid.')

    def test_required_fields(self):
        self.profile.password_reset_key = random_string(16)
        self.profile.password_reset_date = timezone.now()

        self.profile.save()

        page = self.app.get(ext_reverse(
            'accounts:reset', kwargs={
                'profile_pk': self.profile.pk,
                'reset_key': self.profile.password_reset_key
            }
        ))

        self.assertEqual(len(page.forms), 1)

        page = page.form.submit()

        self.assertContains(page, 'This field is required.')

    def test_valid_submission(self):
        self.profile.password_reset_key = random_string(16)
        self.profile.password_reset_date = timezone.now()

        self.profile.save()

        page = self.app.get(ext_reverse(
            'accounts:reset', kwargs={
                'profile_pk': self.profile.pk,
                'reset_key': self.profile.password_reset_key
            }
        ))

        self.assertEqual(len(page.forms), 1)

        page.form['password'] = 'secret123'

        page = page.form.submit()

        self.assertContains(page, 'The password has been successfully changed.')

        # Make sure the user was signed in
        self.assertFalse(isinstance(page.context['user'], AnonymousUser))


class ProfileTests(WebTest):
    fixtures = ('users', 'profiles')

    def setUp(self):
        self.user = User.objects.get(email='acme@example.com')
        self.page = self.app.get(ext_reverse('accounts:profile'), **{'user': self.user})

    def test_status_code(self):
        self.assertEqual(self.page.status_code, 200)

    def test_form_presence(self):
        self.assertEqual(len(self.page.forms), 1)

    def test_required_fields(self):
        self.page.form['full_name'] = ''

        page = self.page.form.submit()

        self.assertContains(page, 'This field is required.')

    def test_valid_submission(self):
        self.page.form['full_name'] = 'Izzy Luke'
        self.page.form['phone'] = '123-456-7890'
        self.page.form['street_address'] = 'St Addr'
        self.page.form['apt_number'] = '321'
        self.page.form['city'] = 'Mississauga'
        self.page.form['country'] = 'Canada'

        page = self.page.form.submit()

        self.assertEqual(self.user.profile.full_name, 'Izzy Luke')
        self.assertEqual(self.user.profile.phone, '1234567890')
        self.assertEqual(self.user.profile.street_address, 'St Addr')
        self.assertEqual(self.user.profile.apt_number, '321')
        self.assertEqual(self.user.profile.city, 'Mississauga')
        self.assertEqual(self.user.profile.country, 'Canada')

        self.assertContains(page, 'Your profile has been successfully.')


class EmailTests(WebTest):
    fixtures = ('users', 'profiles')

    def setUp(self):
        self.user = User.objects.get(email='acme@example.com')
        self.page = self.app.get(ext_reverse('accounts:email'), **{'user': self.user})

    def test_status_code(self):
        self.assertEqual(self.page.status_code, 200)

    def test_form_presence(self):
        self.assertEqual(len(self.page.forms), 1)

    def test_required_fields(self):
        self.page.form['new_email'] = ''

        page = self.page.form.submit()

        self.assertContains(page, 'This field is required.')

    def test_valid_submission(self):
        self.page.form['new_email'] = 'joe@example.com'

        page = self.page.form.submit()

        self.assertEqual(self.user.profile.new_email, 'joe@example.com')
        self.assertEqual(len(self.user.profile.email_change_key), 16)

        self.assertContains(page, "You've an email change request pending confirmation: joe@example.com")


class EmailConfirmationTests(WebTest):
    fixtures = ('users', 'profiles')

    def test_no_pending_request(self):
        user = User.objects.get(email='acme@example.com')

        page = self.app.get(ext_reverse('accounts:change_email', kwargs={
            'profile_pk': user.profile.pk,
            'reset_key': 'xxx'
        }))

        self.assertContains(page, "There's no email change request pending for this account.")

    def test_valid_request(self):
        user = User.objects.get(email='acme@example.com')

        reset_key = random_string(16)

        user.profile.new_email = 'mike@example.com'
        user.profile.email_change_key = reset_key
        user.profile.save()

        self.assertEqual(user.email, 'acme@example.com')

        page = self.app.get(ext_reverse('accounts:change_email', kwargs={
            'profile_pk': user.profile.pk,
            'reset_key': reset_key
        }), auto_follow=True)

        user.refresh_from_db()
        user.profile.refresh_from_db()

        self.assertEqual(user.email, 'mike@example.com')
        self.assertEqual(user.profile.new_email, '')
        self.assertEqual(user.profile.email_change_key, '')

        self.assertContains(page, 'The new email has been set for this account.')


class PasswordTests(WebTest):
    fixtures = ('users', 'profiles')

    def setUp(self):
        self.user = User.objects.get(email='acme@example.com')
        self.page = self.app.get(ext_reverse('accounts:password'), **{'user': self.user})

    def test_status_code(self):
        self.assertEqual(self.page.status_code, 200)

    def test_form_presence(self):
        self.assertEqual(len(self.page.forms), 1)

    def test_required_fields(self):
        self.page.form['password'] = ''
        self.page.form['password_confirmation'] = ''
        self.page.form['current_password'] = ''

        page = self.page.form.submit()

        self.assertContains(page, 'This field is required.', 3)

    def test_incorrect_current_password(self):
        self.page.form['password'] = '123123'
        self.page.form['password_confirmation'] = '123123'
        self.page.form['current_password'] = '123456'

        page = self.page.form.submit()

        self.assertContains(page, 'Current password is invalid.')

    def test_valid_submission(self):
        self.page.form['password'] = '123123'
        self.page.form['password_confirmation'] = '123123'
        self.page.form['current_password'] = 'secret'

        page = self.page.form.submit()

        self.assertContains(page, 'The new password has been set for this account.')
