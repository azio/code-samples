import uuid

from django.db import models


class Profile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    full_name = models.CharField(max_length=60)
    phone = models.CharField(max_length=15, blank=True)
    street_address = models.CharField(max_length=250, blank=True)
    apt_number = models.CharField(max_length=4, blank=True)
    postal_code = models.CharField(max_length=20, blank=True)
    is_email_confirmed = models.BooleanField(default=False)
    activation_key = models.CharField(max_length=16, blank=True)
    password_reset_key = models.CharField(max_length=16, blank=True)
    password_reset_date = models.DateTimeField(null=True, blank=True)
    first_email = models.CharField(max_length=254, blank=True)
    email_change_key = models.CharField(max_length=16, blank=True)
    new_email = models.CharField(max_length=254, blank=True)
    previous_email = models.CharField(max_length=254, blank=True)
    country = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100, blank=True)
    ip_address = models.GenericIPAddressField(default='0.0.0.0')
    private_notes = models.TextField(blank=True)
    is_deleted = models.BooleanField(default=False)
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)