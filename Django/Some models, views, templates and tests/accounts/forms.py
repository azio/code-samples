import re

from django import forms
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from mothership.utils.forms import BaseForm
from accounts.models import Profile


class UserSignUpForm(BaseForm, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserSignUpForm, self).__init__(*args, **kwargs)

        self.fields['password'].widget.attrs['placeholder'] = 'New Password'
        self.fields['email'].required = True
        self.fields['email'].widget.attrs['placeholder'] = 'Email Address'

    def clean_email(self):
        email = self.cleaned_data.get('email')

        # Must be moved to settings.py
        # disposable_email_providers = ('mailinator.com', 'guerrillamail.com', 'getairmail.com', 'fakeinbox.com', 'yopmail.com', '10minutemail.com', 'temp-mail.org', 'dispostable.com', 'mintemail.com', 'mailimate.com', 'spamgourmet.com', 'trashmail.com')

        if email and User.objects.filter(email=email).exclude(pk=self.instance.id).count():
            raise forms.ValidationError('Email is already in use.')
        # elif email.endswith(disposable_email_providers):
        #     raise forms.ValidationError('You may not use a disposable email provider to register.')

        return email

    class Meta:
        model = User
        fields = ('password', 'email')


class ProfileSignUpForm(BaseForm, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileSignUpForm, self).__init__(*args, **kwargs)

        self.fields['full_name'].widget.attrs['placeholder'] = 'Full Name'

    class Meta:
        model = Profile
        fields = ('full_name',)


class LoginForm(BaseForm):
    email = forms.CharField(label='Email', max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Email', 'autofocus': ''}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'placeholder': 'Password'}), min_length=6, max_length=128)


class PasswordRecoveryForm(BaseForm):
    email = forms.EmailField(max_length=254, widget=forms.TextInput(attrs={'placeholder': 'Email Address', 'autofocus': ''}))


class PasswordResetForm(BaseForm, forms.ModelForm):
    """
    When the user forgets his current password and wants to set a new one.
    """

    password = forms.CharField(required=False, widget=forms.PasswordInput(attrs={'placeholder': 'New Password'}), min_length=6, max_length=128)

    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['password'].required = True

    def clean_password(self):
        password = self.cleaned_data.get('password')
        password = make_password(password)

        return password

    class Meta:
        model = User
        fields = ('password',)


class PasswordChangeForm(BaseForm, forms.ModelForm):
    """
    When the user knows his current password and wants to change it.
    """

    password = forms.CharField(widget=forms.PasswordInput(), min_length=6, max_length=128)
    password_confirmation = forms.CharField(widget=forms.PasswordInput(), min_length=6, max_length=128)
    current_password = forms.CharField(widget=forms.PasswordInput(), min_length=6, max_length=128)

    class Meta:
        model = User
        fields = ('password',)

    # def __init__(self, *args, **kwargs):
    #     self.user = kwargs.pop('user', None)
    #     super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_password_confirmation(self):
        password_confirmation = self.cleaned_data.get('password_confirmation')
        password = self.cleaned_data.get('password')

        if password_confirmation != password:
            raise forms.ValidationError('Password and password confirmation must match.')

        return password_confirmation

    def clean_current_password(self):
        current_password = self.cleaned_data.get('current_password')

        if not self.instance.check_password(current_password):
            raise forms.ValidationError('Current password is invalid.')

        return current_password


class ProfileForm(BaseForm, forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('full_name', 'country', 'city', 'phone', 'street_address', 'apt_number', 'postal_code')

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')

        phone = re.sub(r'[-+() ]', '', phone)

        if phone and not phone.isdigit():
            raise forms.ValidationError('The phone number format is incorrect.')

        return phone

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)

        self.fields['phone'].required = False


class EmailForm(BaseForm):
    new_email = forms.EmailField(max_length=254, error_messages={'invalid': 'Enter a valid email.'})
