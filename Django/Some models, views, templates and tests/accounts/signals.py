from django.conf import settings
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from mothership.utils.signals import disable_for_loaddata
from accounts.models import Profile
import uuid

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
@disable_for_loaddata
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


@receiver(pre_save, sender=settings.AUTH_USER_MODEL)
def set_username(sender, instance, **kwargs):
    if instance.username == '':
        instance.username = uuid.uuid4()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
@disable_for_loaddata
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=Profile)
def set_initial_email(sender, instance, created, **kwargs):
    if created:
        instance.first_email = instance.user.email
