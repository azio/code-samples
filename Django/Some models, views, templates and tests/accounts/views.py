from datetime import timedelta

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.gis.geoip2 import GeoIP2
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.utils.translation import ugettext as _
from django.views.generic import View
from geoip2.errors import AddressNotFoundError

from accounts.forms import UserSignUpForm, ProfileSignUpForm, LoginForm, PasswordRecoveryForm, PasswordResetForm, ProfileForm, EmailForm, PasswordChangeForm
from mothership.utils.email import send_email
from mothership.utils.request import get_client_ip
from mothership.utils.string import random_string
from mothership.utils.url import ext_reverse
from organizations.forms import OrganizationForm
from organizations.models import Membership
from .models import Profile


class SignUpView(View):
    user_form_class = UserSignUpForm
    profile_form_class = ProfileSignUpForm
    organization_form_class = OrganizationForm

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(ext_reverse('consoles:goto'))

        account_type = self.kwargs['type']

        user_form = self.user_form_class()
        profile_form = self.profile_form_class()
        organization_form = self.organization_form_class() if self.kwargs['type'] == 'organization' else None

        return render(request, 'accounts/signup.html', {
            'account_type': account_type,
            'user_form': user_form,
            'profile_form': profile_form,
            'organization_form': organization_form,
        })

    def post(self, request, *args, **kwargs):
        user_form = self.user_form_class(request.POST)
        profile_form = self.profile_form_class(request.POST)
        organization_form = self.organization_form_class(request.POST) if self.kwargs['type'] == 'organization' else None

        organization_form_check_passed = True

        if organization_form is not None:
            organization_form_check_passed = organization_form.is_valid()

        if user_form.is_valid() and profile_form.is_valid() and organization_form_check_passed:
            # Create base user
            user = user_form.save()
            user.set_password(user_form.cleaned_data['password'])
            user.save()

            # Associate organization
            if organization_form is not None:
                # Create, but don't save the new author instance
                organization = organization_form.save(commit=False)

                # Save the new instance.
                organization.save()

                Membership.objects.create(
                    user=user,
                    organization=organization,
                    status='accepted',
                    joined_at=timezone.now()
                )

            # Set full name, IP, country, city, activation code and send activation email
            profile = user.profile

            profile.full_name = profile_form.cleaned_data['full_name']

            ip = get_client_ip(request)

            # Try to determine country and city based on user IP
            g = GeoIP2()

            # Country
            try:
                country = g.country(ip)

                if country is not None:
                    profile.country = country['country_name']
            except AddressNotFoundError:
                pass

            # City
            try:
                # Returns a dictionary; {'city': u'Mountain View', 'continent_code': u'NA'...
                city = g.city(ip)

                if city is not None:
                    profile.city = city['city_name']
            except AddressNotFoundError:
                pass

            profile.ip_address = ip
            profile.activation_key = random_string(16)

            profile.save()

            # Send activation email
            send_email(
                _('Activate Your Acme Account'),
                'accounts/emails/activate',
                settings.FROM_EMAIL,
                user.email,
                {
                    'full_name': profile.full_name,
                    'activation_url': 'https://www.example.com/accounts/activate/%s/%s/' % (user.profile.id, profile.activation_key)
                }
            )

            # Render activation required message
            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'success',
                'title': _('Activation Required'),
                'content': _("Your new account has been successfully created. However, it will not be activated until you click the link in the activation email you'll shortly receive at %s.") % user.email
            })

        # Re-render sign up form due to an invalid submission
        return render(request, 'accounts/signup.html', {
            'organization_form': organization_form,
            'user_form': user_form,
            'profile_form': profile_form
        })


class LoginView(View):
    form_class = LoginForm
    template_name = 'accounts/login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(ext_reverse('consoles:goto'))

        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        redirect_to = request.POST.get('next', ext_reverse('consoles:goto'))
        invalid_credentials = False
        form = self.form_class(request.POST)

        # Process form cleaned data
        if form.is_valid():
            # Ensure the user-originating redirection URL is safe
            if not is_safe_url(url=redirect_to, allowed_hosts=request.get_host()):
                redirect_to = settings.LOGIN_REDIRECT_URL

            email = request.POST['email']
            password = request.POST['password']

            user = authenticate(username=email, password=password)

            if user is not None:
                profile = user.profile

                if not profile.is_email_confirmed:
                    # Return a 'account not activated' error message
                    return render(request, 'mothership/naked_message.html', {
                        'app': 'accounts',
                        'type': 'warning',
                        'title': _('Activation Required'),
                        'content': _('This account has not been activated yet. Please check your email to find the activation instructions.')
                    })
                else:
                    login(request, user)

                    return redirect(redirect_to)
            else:
                # Return an 'invalid login' error message.
                invalid_credentials = True

        return render(request, self.template_name, {
            'form': form,
            'invalid_credentials': invalid_credentials
        })


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            logout(request)

        return redirect(ext_reverse('website:home'))


class ActivateView(View):
    def get(self, request, *args, **kwargs):
        profile = get_object_or_404(Profile, pk=self.kwargs['profile_pk'])
        user = profile.user

        if profile.is_email_confirmed:
            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'warning',
                'title': _('Already Activated'),
                'content': _('This account has been already activated.')
            })
        if profile.activation_key == self.kwargs['activation_key']:
            profile.is_email_confirmed = True
            profile.activation_key = ''
            profile.save()

            # user.backend = 'django.contrib.auth.backends.ModelBackend'
            user.backend = 'accounts.backends.EmailBackend'
            login(request, user)

            messages.success(request, _('Your account has been successfully activated.'), extra_tags='account-activated')

            return redirect(ext_reverse('consoles:goto'))
        else:
            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'error',
                'title': _('Invalid Activation Request'),
                'content': _('The activation URL is invalid. Please click on the activation link sent to your email account.')
            })


class RecoverView(View):
    form_class = PasswordRecoveryForm
    template_name = 'accounts/recover.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        invalid_email = False
        form = self.form_class(request.POST)

        if form.is_valid():
            email = form.cleaned_data['email']

            try:
                user = User.objects.get(email=email)

                if user.is_active:
                    profile = user.profile

                    profile.password_reset_key = random_string(16)
                    profile.password_reset_date = timezone.now()

                    profile.save()

                    send_email(
                        # Translators: Email Subject
                        _('Reset Your Acme Password'),
                        'accounts/emails/recover',
                        settings.FROM_EMAIL,
                        user.email,
                        {
                            'full_name': profile.full_name,
                            'password_reset_url': 'https://www.example.com/accounts/reset/%s/%s/' % (user.profile.id, profile.password_reset_key)
                        }
                    )

                    return render(request, 'mothership/naked_message.html', {
                        'app': 'accounts',
                        'type': 'info',
                        'title': _('Check Your Email'),
                        'content': _('Instructions for resetting your password have just been emailed to you.')
                    })
                else:
                    # Return a 'disabled account' error message
                    return render(request, 'mothership/naked_message.html', {
                        'app': 'accounts',
                        'type': 'error',
                        'title': _('Account Suspended'),
                        'content': _('This account has been suspended. This may be caused by either a violation of the Terms of Use or for the purpose of verifying your membership.')
                    })
            except User.DoesNotExist:
                # Return an 'invalid email' error message.
                invalid_email = True

        return render(request, self.template_name, {
            'form': form,
            'invalid_email': invalid_email
        })


class ResetView(View):
    form_class = PasswordResetForm
    template_name = 'accounts/reset.html'
    user = None
    profile = None

    def dispatch(self, request, *args, **kwargs):
        # Make sure user exists and is active, reset key not expired and the provided key is valid
        try:
            self.profile = Profile.objects.get(pk=self.kwargs['profile_pk'])
            self.user = self.profile.user

            if self.user.is_active:
                # Did the request expire?
                if self.profile.password_reset_date < timezone.now() - timedelta(days=1):
                    return render(request, 'mothership/naked_message.html', {
                        'app': 'accounts',
                        'type': 'error',
                        'title': _('Request Expired'),
                        'content': _('This password reset request has expired. Feel free to send a new request if you still desire to change it.')
                    })

                # Is the key correct?
                if self.profile.password_reset_key != self.kwargs['reset_key']:
                    return render(request, 'mothership/naked_message.html', {
                        'app': 'accounts',
                        'type': 'error',
                        'title': _('Invalid Reset Key'),
                        'content': _('The provided reset key is invalid or has been previously used.')
                    })
            else:
                # Return a 'disabled account' error message
                return render(request, 'mothership/naked_message.html', {
                    'app': 'accounts',
                    'type': 'error',
                    'title': _('Account Suspended'),
                    'content': _('This account has been suspended. This may be caused by either a violation of the Terms of Use or for the purpose of verifying your membership.')
                })
        except Profile.DoesNotExist:
            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'error',
                'title': _('Invalid Key'),
                'content': _('The reset URL is invalid.')
            })

        return super(ResetView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class()

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, instance=self.user)

        if form.is_valid():
            # For an unknown reason -yet- commenting out the following line has no effect.  The password will still be saved.
            # However, commenting out self.profile.save() below prevent the profile fields from being updated!
            form.save()

            self.profile.password_reset_key = ''
            self.profile.save()

            self.user.backend = 'accounts.backends.EmailBackend'
            login(request, self.user)

            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'success',
                'title': _('Password Changed'),
                'content': _('The password has been successfully changed. <a href="%s">Continue to console</a>.' % ext_reverse('consoles:goto'))
            })

        return render(request, self.template_name, {'form': form})


@method_decorator(login_required, name='dispatch')
class ProfileView(View):
    form_class = ProfileForm
    template_name = 'accounts/profile.html'
    form_template_name = 'accounts/forms/profile.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(instance=self.request.user.profile)

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, instance=self.request.user.profile)

        if form.is_valid():
            form.save()

            messages.success(self.request, _('Your profile has been successfully.'), extra_tags='profile-updated')

        return render(request, self.form_template_name if 'ic-request' in request.POST else self.template_name, {
            'form': form
        })


@method_decorator(login_required, name='dispatch')
class EmailView(View):
    form_class = EmailForm
    template_name = 'accounts/email.html'
    form_template_name = 'accounts/forms/email.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            profile = request.user.profile

            profile.email_change_key = random_string(16)
            profile.new_email = form.cleaned_data['new_email']
            profile.save()

            send_email(
                _('Changing Your Email with Acme'),
                'accounts/emails/email_change',
                settings.FROM_EMAIL,
                profile.user.email,
                {
                    'full_name': profile.full_name,
                    'change_email_url': 'https://www.example.com/accounts/change-email/%s/%s/' % (profile.id, profile.email_change_key)
                }
            )

        return render(request, self.form_template_name if 'ic-request' in request.POST else self.template_name, {
            'form': form
        })


class ConfirmEmailView(View):
    template_name = 'accounts/email.html'

    def get(self, request, *args, **kwargs):
        profile = get_object_or_404(Profile, pk=self.kwargs['profile_pk'])
        user = profile.user

        if not profile.new_email or user.email == profile.new_email:
            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'error',
                'title': _('Request Not Found'),
                'content': _('There\'s no email change request pending for this account.')
            })

        if profile.email_change_key == kwargs['reset_key']:
            profile.previous_email = user.email
            user.email = profile.new_email
            user.save()

            profile.new_email = ''
            profile.email_change_key = ''
            profile.save()

            user.backend = 'accounts.backends.EmailBackend'
            login(request, user)

            messages.success(request, 'The new email has been set for this account.', extra_tags='email-changed')

            return redirect('accounts:email')
        else:
            return render(request, 'mothership/naked_message.html', {
                'app': 'accounts',
                'type': 'error',
                'title': _('Invalid Reset URL'),
                'content': _('The link followed to change the email address is invalid')
            })


@method_decorator(login_required, name='dispatch')
class PasswordView(View):
    form_class = PasswordChangeForm
    template_name = 'accounts/password.html'
    form_template_name = 'accounts/forms/password.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, instance=request.user)

        if form.is_valid():
            request.user.set_password(form.cleaned_data['password'])
            request.user.save()

            # User gets logged out on password change
            request.user.backend = 'accounts.backends.EmailBackend'
            login(request, request.user)

            messages.success(request, 'The new password has been set for this account.', extra_tags='password-changed')

        return render(request, self.form_template_name if 'ic-request' in request.POST else self.template_name, {
            'form': form
        })
