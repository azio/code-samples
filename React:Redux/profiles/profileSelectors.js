export const getIsVerifying = (state) => state.profiles.isVerifying

export const getList = (state) => state.profiles.list