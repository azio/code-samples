import { Realm } from '../core/globals'
import { DuplicateProfileException } from '../core/exceptions'
import { generateUniqueId } from '../utils/string'
import { ApplicationSchema, ProfileSchema, NetworkSchema } from './profileSchemas'
import { UserGroupSchema, UserSchema } from '../users/userSchemas'


export const createProfileByUsernameAndPassword = (network, fullName, username, password) => {
  return Realm.open({ schema: [ApplicationSchema, ProfileSchema, NetworkSchema, UserGroupSchema, UserSchema] })
    .then(realm => {
      // Writes in Realm are synchronous, so we can simply call a callback after the write to indicate success.
      // On failure, a write will throw an exception. 
      // https://github.com/realm/realm-js/issues/594#issuecomment-246697647

      // The try/catch block are not necessarily needed, but it’s good practice as we might use the repository outside of action creators (that already handle the exceptions).
      try {
        const networkRecord = realm.objects('Network').filtered(`name ==[c] "${network}"`)[0]

        const existingProfile = realm.objects('Profile').find(profile => profile.username === username)

        if (existingProfile !== undefined)
          throw new DuplicateProfileException()

        let profileRecord = null

        realm.write(() => {
          profileRecord = realm.create('Profile', {
            id: generateUniqueId(),
            fullName,
            username,
            password,
            imageUrl: 'imageUrl',
            connectedAt: new Date(),
            network: networkRecord
          })
        })

        return profileRecord
      }
      catch (exception) {
        // Rethrow so returned Promise is rejected
        throw exception
      }
    })
}


export const fetchProfiles = () => {
  return Realm.open({ schema: [NetworkSchema, ApplicationSchema, ProfileSchema, UserGroupSchema, UserSchema] })
    .then(realm => {
      return realm.objects('Profile')
    })
}
