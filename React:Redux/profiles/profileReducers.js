import { combineReducers } from 'redux'
import { ADD_NETWORK_STEPS, ADD_PROFILE, CLOSE_ADD_PROFILE_MODAL, OPEN_ADD_PROFILE_MODAL, SAVE_PROFILE_FAILURE, SAVE_PROFILE_REQUEST, SAVE_PROFILE_SUCCESS, RECEIVE_PROFILES } from '../constants/actionTypes'


const addProfileStep = (state = ADD_NETWORK_STEPS.NETWORK_SELECTION, action) => {
  // This could be made shorter!
  switch (action.type) {
    case ADD_NETWORK_STEPS.INSTAGRAM_FORM:
      return ADD_NETWORK_STEPS.INSTAGRAM_FORM

    case ADD_NETWORK_STEPS.TWITTER_AUTOMATIC_FORM:
      return ADD_NETWORK_STEPS.TWITTER_AUTOMATIC_FORM

    case ADD_NETWORK_STEPS.TWITTER_MANUAL_FORM:
      return ADD_NETWORK_STEPS.TWITTER_MANUAL_FORM

    case ADD_NETWORK_STEPS.NETWORK_SELECTION:
      return ADD_NETWORK_STEPS.NETWORK_SELECTION

    default:
      return state
  }
}

const isProfileModalOpen = (state = false, action) => {
  switch (action.type) {
    case CLOSE_ADD_PROFILE_MODAL:
    case SAVE_PROFILE_SUCCESS:
      return false

    case OPEN_ADD_PROFILE_MODAL:
      return true

    default:
      return state
  }
}

const isVerifying = (state = false, action) => {
  switch (action.type) {
    case SAVE_PROFILE_REQUEST:
      return true

    case SAVE_PROFILE_FAILURE:
    case SAVE_PROFILE_SUCCESS:
      return false

    default:
      return state
  }
}


const list = (state = [], action) => {
  switch (action.type) {
    case ADD_PROFILE:
      return [
        ...state,
        action.profileRecord
      ]
    case RECEIVE_PROFILES:
      return action.response
    default:
      return state
  }
}


const profileReducers = combineReducers({
  addProfileStep,
  isProfileModalOpen,
  isVerifying,
  list
})


export default profileReducers