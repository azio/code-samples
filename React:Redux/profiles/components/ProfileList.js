import React from 'react'
import { Button, Card, Container, Header, Icon, Message, Segment } from 'semantic-ui-react'
import HelpModal from '../../help/components/HelpModal'
import ProfileFormContainer from './ProfileFormContainer'


export default function ProfileList({ entries }) {
  let entryList = null

  if (entries) {
    entryList = <Card.Group>
      {entries.map((entry, index) =>
        <Card key={index}>
          <Card.Content>
            <div style={{ color: entry.network.color, float: 'right' }}>
              <Icon name={entry.network.icon} size='big' />
            </div>
            <Card.Header>
              {entry.fullName}
            </Card.Header>
            <Card.Meta>
              @{entry.username}
            </Card.Meta>
          </Card.Content>
          <Card.Content extra>
            <div className='ui two buttons'>
              <Button basic color='green'>Refresh</Button>
              <Button basic color='red'>Remove</Button>
            </div>
          </Card.Content>
        </Card>
      )}
    </Card.Group>
  } else {
    entryList = <Container text>
      <Message>
        <Message.Header>
          Add Profiles to Get Started
        </Message.Header>
        <p>
          You haven&apos;t added any of your social media profiles for Sociarella to manage.
        </p>
      </Message>
    </Container>
  }

  return (
    <div id="profile-list">
      <Segment basic clearing>
        <Header as='h3' floated='left'>
          Social Profiles
        </Header>

        <HelpModal component="ProfileHelp" title="Social Profiles" Trigger={props => <Button floated='right' icon='info' onClick={props.handleOpen} circular basic />} />
      </Segment>

      <Segment basic clearing>
        <ProfileFormContainer />
      </Segment>

      {entryList}
    </div>
  )
}