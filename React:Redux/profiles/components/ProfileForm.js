import React, { Component } from 'react'
import { ADD_NETWORK_STEPS } from '../../constants/actionTypes'
import { Modal } from 'semantic-ui-react'
import ModalHeaderContent from '../../common/ModalHeaderContent'
import AvailableNetworks from './AvailableNetworks'
import StandardLoginForm from './StandardLoginForm'


class ProfileForm extends Component {
  render() {
    // handleSubmit amd submitting are injected into props by Redux Form
    const { addProfileStep, handleModalClose, handleNetworkSelection, handleSaveProfile, isProfileModalOpen, isVerifying, trigger } = this.props

    let stepComponent = null

    if (addProfileStep === ADD_NETWORK_STEPS.NETWORK_SELECTION)
      stepComponent = <AvailableNetworks onSelect={handleNetworkSelection} />
    else if (addProfileStep === ADD_NETWORK_STEPS.INSTAGRAM_FORM) {
      stepComponent = <StandardLoginForm
        intro='Instagram account credentials are required to simulate mobile app posting.'
        isVerifying={isVerifying}
        network='instagram'
        handleSaveProfile={handleSaveProfile}
      />
    }
    else if (addProfileStep === ADD_NETWORK_STEPS.TWITTER_AUTOMATIC_FORM)
      stepComponent = <StandardLoginForm network='twitter' />
    else if (addProfileStep === ADD_NETWORK_STEPS.TWITTER_MANUAL_FORM)
      stepComponent = 'xxx'


    // Mount the modal inside the tab's pane to hide it when the tab is switched
    const profileListElement = document.getElementById('profile-list');

    return (
      <Modal
        trigger={trigger}
        open={isProfileModalOpen}
        onClose={handleModalClose}
        dimmer='inverted'
        mountNode={profileListElement}
      >
        <Modal.Header style={{ paddingBottom: 10 }}>
          {/*Add New Profile*/}
          <ModalHeaderContent title='Add New Profile' onClose={handleModalClose} />
        </Modal.Header>
        <Modal.Content scrolling>
          <Modal.Description>
            {stepComponent}
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )
  }
}

export default ProfileForm