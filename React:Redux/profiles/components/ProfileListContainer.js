import React, { Component } from 'react'
import { connect } from 'react-redux'
import ProfileList from './ProfileList'
import * as profileActions from '../profileActions'
import * as profileRepos from '../profileRepositories'
import * as profileSelectors from '../profileSelectors'


class ProfileListContainer extends Component {
  componentDidMount() {
    this.fetchData()
  }

  // componentDidUpdate(prevProps) {
  //   this.fetchData()
  // }

  async fetchData() {
    const { receiveProfiles } = this.props
    const profiles = await profileRepos.fetchProfiles()

    receiveProfiles(profiles)
  }

  render() {
    const { entries } = this.props

    return (
      <ProfileList
        entries={entries}
      />
    )
  }
}


const mapStateToProps = (state) => ({
  entries: profileSelectors.getList(state)
})

const mapDispatchToProps = {
  receiveProfiles: profileActions.receiveProfiles
}


export default connect(mapStateToProps, mapDispatchToProps)(ProfileListContainer)