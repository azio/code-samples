import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Dimmer, Form, Loader, Message, Segment } from 'semantic-ui-react'
import { reduxForm, Field, SubmissionError } from 'redux-form'
import { DuplicateProfileException, OfflineException } from '../../core/exceptions'
import formValidator from '../../core/formValidator'
import FormField from '../../common/FormField'
import FormSave from '../../common/FormSave'


const propTypes = {
  intro: PropTypes.string.isRequired,
  isVerifying: PropTypes.bool.isRequired,
  network: PropTypes.string.isRequired,
  handleSaveProfile: PropTypes.func.isRequired
}


const constraints = {
  username: { presence: true },
  password: { presence: true }
}


class StandardLoginForm extends Component {
  submit(values) {
    const submissionResult = this.props.handleSaveProfile({ ...values, network: this.props.network })().catch((error) => {

      let errorObject = {}

      if (error instanceof OfflineException || error instanceof DuplicateProfileException)
        errorObject['_error'] = error['message']

      if (error.message.includes('username'))
        errorObject['username'] = error.message
      else if (error.message.includes('password'))
        errorObject['password'] = error.message

      throw new SubmissionError(errorObject)
    })

    return submissionResult
  }

  render() {
    const { error, handleSubmit, intro, isVerifying, submitting } = this.props

    return (
      <Segment basic>
        <Dimmer active={isVerifying} inverted>
          <Loader indeterminate>Verifying Credentials</Loader>
        </Dimmer>

        <p>{intro}</p>

        {error && <Message negative>
          <p>{error}</p>
        </Message>}

        <Form onSubmit={handleSubmit(this.submit.bind(this))}>
          <Field as={Form.Input} autoFocus component={FormField} label="Username" name="username" width={8} />
          <Field as={Form.Input} component={FormField} label="Password" name="password" type="password" width={8} />
          <FormSave caption='Connect Profile' submitting={submitting} />
        </Form>
      </Segment>
    )
  }
}


StandardLoginForm.propTypes = propTypes


export default reduxForm({
  form: 'StandardLoginForm',
  validate: formValidator(constraints)
})(StandardLoginForm)
