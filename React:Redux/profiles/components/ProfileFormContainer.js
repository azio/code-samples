import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Icon } from 'semantic-ui-react'
import ProfileForm from './ProfileForm'
import * as profileActions from '../profileActions'
import * as profileSelectors from '../profileSelectors'


class ProfileFormContainer extends Component {
  render() {
    const {
      addProfileStep,
      closeAddProfileModal,
      openAddProfileModal,
      saveProfile,
      selectNetwork,
      isProfileModalOpen,
      isVerifying
    } = this.props

    return (
      <ProfileForm
        trigger={
          <Button
            basic
            color='black'
            floated='right'
            onClick={openAddProfileModal}
          >
            <Icon name='plus' /> Add New Profile
          </Button>
        }
        addProfileStep={addProfileStep}
        handleModalClose={closeAddProfileModal}
        handleModalOpen={openAddProfileModal}
        handleSaveProfile={saveProfile}
        handleNetworkSelection={selectNetwork}
        isProfileModalOpen={isProfileModalOpen}
        isVerifying={isVerifying}
      />
    )
  }
}


const mapStateToProps = (state) => ({
  addProfileStep: state.profiles.addProfileStep,
  isProfileModalOpen: state.profiles.isProfileModalOpen,
  isVerifying: profileSelectors.getIsVerifying(state)
})

const mapDispatchToProps = {
  closeAddProfileModal: profileActions.closeAddProfileModal,
  openAddProfileModal: profileActions.openAddProfileModal,
  saveProfile: profileActions.saveProfile,
  selectNetwork: profileActions.selectNetwork
}


export default connect(mapStateToProps, mapDispatchToProps)(ProfileFormContainer)