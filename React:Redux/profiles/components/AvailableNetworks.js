import React from 'react'
import PropTypes from 'prop-types'
import { Button, Popup } from 'semantic-ui-react'

const propTypes = {
  onSelect: PropTypes.func.isRequired
}

function underConstructionMessage(network) {
  return `${network} integration is in the works.\n\nEnjoy Instagram features in the meantime.`
}

function AvailableNetworks({ onSelect }) {
  return (
    <div>
      <p>Select a social media network to proceed.</p>

      <Popup
        content='Instagram'
        position='bottom center'
        trigger={<Button color='instagram' icon='instagram' size='massive' onClick={() => onSelect('instagram')} />}
      />

      <Popup
        content='Twitter (Coming Soon)'
        position='bottom center'
        trigger={<Button color='twitter' icon='twitter' size='massive' onClick={() => alert(underConstructionMessage('Twitter'))} />}
      />

      <Popup
        content='Facebook (Coming Soon)'
        position='bottom center'
        trigger={<Button color='facebook' icon='facebook' size='massive' onClick={() => alert(underConstructionMessage('Facebook'))} />}
      />

      <Popup
        content='Google Plus (Coming Soon)'
        position='bottom center'
        trigger={<Button color='google plus' icon='google plus' size='massive' onClick={() => alert(underConstructionMessage('Google Plus'))} />}
      />

      <Popup
        content='LinkedIn (Coming Soon)'
        position='bottom center'
        trigger={<Button color='linkedin' icon='linkedin' size='massive' onClick={() => alert(underConstructionMessage('LinkedIn'))} />}
      />

      <Popup
        content='YouTube (Coming Soon)'
        position='bottom center'
        trigger={<Button color='youtube' icon='youtube' size='massive' onClick={() => alert(underConstructionMessage('YouTube'))} />}
      />
    </div>
  )
}

AvailableNetworks.propTypes = propTypes

export default AvailableNetworks