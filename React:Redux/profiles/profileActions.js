import { ADD_NETWORK_STEPS, ADD_PROFILE, CLOSE_ADD_PROFILE_MODAL, OPEN_ADD_PROFILE_MODAL, SAVE_PROFILE_FAILURE, SAVE_PROFILE_REQUEST, SAVE_PROFILE_SUCCESS, RECEIVE_PROFILES } from '../constants/actionTypes'
import { OfflineException } from '../core/exceptions'
import createSession from '../users/userApis/createSession'
import getAccount from '../users/userApis/getAccount'
import * as profileRepos from './profileRepositories'
import { showNotification, hideNotification } from '../notifications/notificationActions'


export const openAddProfileModal = () => ({
  type: OPEN_ADD_PROFILE_MODAL
})


export const closeAddProfileModal = () => dispatch => {
  dispatch(selectNetwork(ADD_NETWORK_STEPS.NETWORK_SELECTION)) // Redirects the user to the main screen of the wizard
  dispatch(cancelProfileSave()) // Removes the loading indicator

  return dispatch({
    type: CLOSE_ADD_PROFILE_MODAL
  })
}


export const selectNetwork = (network) => {
  let type

  switch (network) {
    case 'instagram':
      type = ADD_NETWORK_STEPS.INSTAGRAM_FORM
      break

    case 'twitter':
      type = ADD_NETWORK_STEPS.TWITTER_AUTOMATIC_FORM
      break

    default:
      type = ADD_NETWORK_STEPS.NETWORK_SELECTION
  }

  return {
    type
  }
}


export const requestProfileSave = () => ({
  type: SAVE_PROFILE_REQUEST
})


export const processProfileSave = () => ({
  type: SAVE_PROFILE_SUCCESS
})


export const cancelProfileSave = () => ({
  type: SAVE_PROFILE_FAILURE
})


export const saveProfile = ({ network, username, password }) => (dispatch, getState) => {
  dispatch(requestProfileSave())

  const { isOnline } = getState().settings

  return async () => {
    if (!isOnline) {
      dispatch(cancelProfileSave())

      throw new OfflineException()
    }

    const session = await createSession(network, username, password)
      .catch((error) => {
        dispatch(cancelProfileSave())

        throw error
      })

    const account = await getAccount(network, session)
      .catch((error) => {
        dispatch(cancelProfileSave())

        throw error
      })

    const profileRecord = await profileRepos.createProfileByUsernameAndPassword('instagram', account.fullName, username, password)
      .catch((error) => {
        dispatch(cancelProfileSave())

        throw error
      })

    // dispatch(closeAddProfileModal())
    dispatch(processProfileSave())

    return dispatch(addProfile(profileRecord))
  }
}


export const receiveProfiles = (response) => ({
  type: RECEIVE_PROFILES,
  response
})


export const addProfile = (profileRecord) => dispatch => {
  dispatch(showNotification('profileAdded'))

  setTimeout(() => {
    dispatch(hideNotification('profileAdded'))
  }, 3000)

  return dispatch({
    type: ADD_PROFILE,
    profileRecord
  })
}