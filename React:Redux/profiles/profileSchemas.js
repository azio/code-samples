export const NetworkSchema = {
  name: 'Network',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    icon: 'string',
    color: 'string',
    // profiles: { type: 'list', objectType: 'Profile' }
    profiles: { type: 'linkingObjects', objectType: 'Profile', property: 'network' }
  }
}


export const ApplicationSchema = {
  name: 'Application',
  primaryKey: 'id',
  properties: {
    id: 'int',
    consumerKey: 'string',
    consumerSecret: 'string',
    createdAt: 'date',
    createdBy: 'User',
    // profiles: { type: 'list', objectType: 'Profile' }
  }
}


export const ProfileSchema = {
  name: 'Profile',
  primaryKey: 'id',
  properties: {
    id: 'int',
    fullName: 'string',
    username: 'string',
    password: { type: 'string', optional: true },
    networkId: { type: 'string', optional: true },
    accessToken: { type: 'string', optional: true },
    accessTokenSecret: { type: 'string', optional: true },
    imageUrl: { type: 'string', optional: true },
    connectedAt: 'date',
    accessTokenExpiresAt: { type: 'date', optional: true },
    // posts: { type: 'linkingObjects', objectType: 'Post', property: 'profile' },
    network: 'Network',
    // applications: { type: 'linkingObjects', objectType: 'Application', property: 'profiles' },
    // networks: { type: 'linkingObjects', objectType: 'Network', property: 'profiles' }
  }
}