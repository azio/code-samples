import React from 'react'
import PropTypes from 'prop-types';
// import Dashboard from '../../dashboard'
import Home from '../../home'
import ProfileListContainer from '../../profiles/components/ProfileListContainer'
import PostFormContainer from '../../posts/components/PostFormContainer'


const propTypes = {
  tabList: PropTypes.array.isRequired,
  handleItemClick: PropTypes.func.isRequired
}


export default function Panes({ tabList, handleItemClick }) {
  const componentByName = {
    // Dashboard: <Dashboard />,
    Home: <Home handleItemClick={handleItemClick} />,
    ProfileListContainer: <ProfileListContainer />,
    PostFormContainer: <PostFormContainer />,
  }

  const paneList = tabList.map((tab) =>
    <div key={tab.id} className={tab.active ? '' : 'hidden'}>
      {componentByName[tab.component]}
    </div>
  )

  return (
    <div style={{ marginTop: '5em' }}>
      {paneList}
    </div>
  )
}


Panes.propTypes = propTypes
