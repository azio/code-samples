import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Grid } from 'semantic-ui-react'
import Panes from '../../tabs/components/Panes'
import Tabs from '../../tabs/components/Tabs'
import * as tabActions from '../../tabs/tabActions'


class TabsContainer extends Component {
  render() {
    const { tabs, addTab, switchTab, closeTab } = this.props

    return (
      <Grid padded>
        <Grid.Row>
          <Grid.Column width={16}>
            <Tabs tabList={tabs} onClick={switchTab} onCloseClick={closeTab} />

            <Panes tabList={tabs} handleItemClick={addTab} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}


const mapStateToProps = (state) => ({
  tabs: state.tabs,
  // currentPath: tabSelectors.getCurrentPath(state)
})

// const mapDispatchToProps = {
//   addTab: tabActions.addTab,
//   closeTab: tabActions.closeTab,
//   switchTab: tabActions.switchTab
// }


export default connect(mapStateToProps, tabActions)(TabsContainer)