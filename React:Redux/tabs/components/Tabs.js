import React from 'react'
import { Icon, Menu } from 'semantic-ui-react'

export default function Tabs({ tabList, onClick, onCloseClick }) {
  // const handleItemClick = (e, {name}) => {
  //
  // }

  const menuItems = tabList.map((tab) =>
    <Menu.Item
      key={tab.id}
      // name={tab.id.toString()}
      active={tab.active}
      onClick={() => onClick({ id: tab.id })}
      style={{ paddingRight: tab.closable ? '0.625em' : 0 }}
    >
      <Icon name={tab.icon} />
      {tab.caption}

      {/*<Label>15</Label>*/}

      <span style={{ paddingLeft: tab.closable ? '2em' : '1em' }}></span>

      {/* stopPropagation below is to stop the parent Menu.Item onClick from being fired too */}
      {tab.closable && <Icon
        name='close'
        style={{ opacity: 0.1 }}
        onClick={(event) => { onCloseClick({ id: tab.id }) && event.stopPropagation() }}
      />}
    </Menu.Item>
  )

  const mainTabsStyle = {
    backgroundColor: 'white',
    left: 0,
    overflowX: 'scroll',
    overflowY: 'hidden',
    paddingTop: '1em',
    position: 'fixed',
    top: 0,
    width: '100%',
    zIndex: 2147483647
  }

  return (
    <Menu className="main-tabs" style={mainTabsStyle} tabular>
      {menuItems}
    </Menu>
  )
}