import { createSelector } from 'reselect'


const getTabs = (state) => state.tabs


export const getTabByComponent = (state, component) => (getTabs(state).find(tab => tab.component === component))


export const getActiveTab = createSelector(
  getTabs,
  (tabs) => tabs.find(tab => tab.active)
)
