import { v4 as uuid4 } from 'uuid'
import { ADD_TAB, CLOSE_TAB, DESELECT_TABS, SWITCH_TAB } from '../constants/actionTypes'


const defaultTab = {
  id: uuid4(),
  icon: 'home',
  component: 'Home',
  path: '/',
  // active: true,
  active: false,
  closable: false,
  singular: true
}


// const profileListTab = {
//   id: uuid4(),
//   icon: 'users',
//   caption: 'Profile List',
//   component: 'ProfileListContainer',
//   path: '/profiles',
//   active: true,
//   closable: true,
//   singular: true
// }


const postFormTab = {
  id: uuid4(),
  icon: 'edit',
  caption: 'Submit a Post',
  component: 'PostFormContainer',
  path: '/post',
  active: true,
  closable: true
}


export default (state = [defaultTab, postFormTab], action) => {
  // const tabReducer = (state = [defaultTab], action) => {
  switch (action.type) {
    case ADD_TAB:
      return [
        ...state,
        {
          id: action.id,
          icon: action.icon,
          caption: action.caption,
          component: action.component,
          path: action.path,
          active: action.active,
          closable: action.closable,
          singular: action.singular
        }
      ]

    case CLOSE_TAB:
      return state.filter(tab => tab.id !== action.id)

    case DESELECT_TABS:
      return state.map((tab) => ({ ...tab, active: false }))

    case SWITCH_TAB:
      return state.map((tab) => {
        if (typeof action.id !== 'undefined')
          return tab.id === action.id ? { ...tab, active: true } : tab
        else if (typeof action.path !== 'undefined')
          return tab.path === action.path ? { ...tab, active: true } : tab
        else
          return tab
      })

    default:
      return state
  }
}