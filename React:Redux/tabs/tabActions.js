import { v4 as uuid4 } from 'uuid'
import { ADD_TAB, CLOSE_TAB, DESELECT_TABS, SWITCH_TAB } from '../constants/actionTypes'
import { getActiveTab, getTabByComponent, } from '../tabs/tabSelectors'


// The parameters are provided as object in order to be able to omit some of the default ones
export const addTab = ({ icon, caption, component, path, active = true, closable = true, singular = false }) => (dispatch, getState) => {
  if (active)
    dispatch(deselectTabs())

  // The Add Tab Action Creator doesn't necessarily creates the tab.
  // If singular is set to true and the tab path exists, we simply set it as the active tab.
  if (singular) {
    const requestedTab = getTabByComponent(getState(), component)

    if (requestedTab !== undefined)
      return dispatch(switchTab({ id: requestedTab.id }))
  }

  return dispatch({
    type: ADD_TAB,
    id: uuid4(),
    icon,
    caption,
    component,
    path,
    active,
    closable,
    singular
  })
}

export const closeTab = ({ id, path }) => (dispatch, getState) => {
  const currentTab = getActiveTab(getState())

  // Switch to home only if the current active tab is being closed
  if (currentTab.id === id)
    dispatch(switchTab({ path: '/' }))

  return dispatch({
    type: CLOSE_TAB,
    id: id
  })
}

export const deselectTabs = () => ({
  type: DESELECT_TABS
})


export const switchTab = ({ id, path }) => dispatch => {
  dispatch(deselectTabs())

  // A tab may be switched by path or ID
  return dispatch({
    type: SWITCH_TAB,
    id: id,
    path: path
  })
}